/* eslint-disable react/jsx-props-no-spreading */
import React from 'react';
import { Progress } from 'reactstrap';
import PropTypes from 'prop-types';

function ProgressBar(opts) {
  return <Progress {...opts} />;
}
ProgressBar.propTypes = {
  opts: PropTypes.shape({
    value: PropTypes.number,
    min: PropTypes.number,
    max: PropTypes.number,
    animated: PropTypes.bool,
  }),
};
ProgressBar.defaultProps = {
  opts: { value: 60, min: 0, max: 100, animated: true },
};
export default ProgressBar;
