import React from 'react';
import PropTypes from 'prop-types';

function ImageThumbnail({ imageId, alt, customStyle }) {
  return <img style={customStyle} src={`${process.env.REACT_APP_BACKEND_URL}/api/file/image/${imageId}`} alt={alt} />;
}

ImageThumbnail.propTypes = {
  imageId: PropTypes.string.isRequired,
  alt: PropTypes.string.isRequired,
  customStyle: PropTypes.shape({}).isRequired,
};
export default ImageThumbnail;
