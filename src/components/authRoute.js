/* eslint-disable react/jsx-props-no-spreading */
/* eslint-disable react/destructuring-assignment */
/* eslint-disable camelcase */
import React from 'react';
import { Route, Redirect } from 'react-router-dom';
import PropTypes from 'prop-types';
import * as alerts from '../utils/toastr';
import { decodeLocalUser } from '../helpers/auth';

function AuthRoute({ Component, path, exact = false, role }) {
  if (!role) {
    return <Route exact={exact} path={path} render={(props) => <Component {...props} />} />;
  }
  const user = decodeLocalUser();

  return (
    <Route
      exact={exact}
      path={path}
      render={(props) => {
        if (!user) {
          alerts.errorAlert('Not connected', 'You must be connected to access this page');
          return <Redirect to="/login" />;
        }
        if (user.role !== role) {
          alerts.errorAlert('Unauthorized', 'You are not authorized to access this page');
          return <Redirect to="/login" />;
        }
        return <Component {...props} />;
      }}
    />
  );
}
AuthRoute.propTypes = {
  Component: PropTypes.shape({}).isRequired,
  path: PropTypes.string.isRequired,
  exact: PropTypes.bool,
  role: PropTypes.string,
};
AuthRoute.defaultProps = {
  exact: false,
  role: null,
};
export default AuthRoute;
