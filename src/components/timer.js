import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';

function Timer({ countdown, setValue }) {
  const [timeLeft, setTimeLeft] = useState(countdown);
  useEffect(() => {
    setTimeLeft(countdown);
  }, []);
  useEffect(() => {
    const tick = setTimeout(() => {
      setTimeLeft(timeLeft - 1);
      setValue(timeLeft - 1);
    }, 1000);
    return () => {
      clearTimeout(tick);
    };
  });
  return <b>{countdown > 0 ? <b>{` ${timeLeft} seconds`}</b> : ' ∞'}</b>;
}
Timer.propTypes = {
  countdown: PropTypes.number.isRequired,
  setValue: PropTypes.func.isRequired,
};
export default Timer;
