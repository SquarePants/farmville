import io from 'socket.io-client';

export default (() => {
  let socketRepo;

  function createSocket() {
    socketRepo = io(process.env.REACT_APP_BACKEND_URL);
    // console.log("Socket Created ",socketRepo);
    return socketRepo;
  }

  return {
    getInstance() {
      if (!socketRepo) {
        socketRepo = createSocket();
      }
      return socketRepo;
    },
  };
})();
