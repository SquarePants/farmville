import { createStore, applyMiddleware } from 'redux';
import createSagaMiddleware from 'redux-saga';
import reduxLogger from 'redux-logger';
import rootReducer from './reducers';
import appSaga from '../containers/App/saga';
import productSaga from '../containers/Product/saga';
import forestSaga from '../containers/Forest/saga';

const sagaMiddleware = createSagaMiddleware();

export default function configureStore(initialState) {
  const middlewares = [sagaMiddleware, reduxLogger];
  const store = createStore(rootReducer, initialState, applyMiddleware(...middlewares));
  sagaMiddleware.run(appSaga);
  sagaMiddleware.run(productSaga);
  sagaMiddleware.run(forestSaga);
  return store;
}
