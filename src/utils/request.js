/**
 * Parses the JSON returned by a network request
 *
 * @param  {object} response A response from a network request
 *
 * @return {object}          The parsed JSON from the request
 */
const parseJSON = (response) => {
  if (response.status === 204 || response.status === 205) {
    return null;
  }
  return response.json();
};

/**
 * Checks if a network request came back fine, and throws an error if not
 *
 * @param  {object} response   A response from a network request
 *
 * @return {object|undefined} Returns either the response, or throws an error
 */
const checkStatus = (response) => {
  if ((response.status >= 200 && response.status < 300) || response.status === 422 || response.status === 404) {
    return response;
  }
  const error = new Error(response.statusText);
  error.response = response;
  error.status = response.status;
  throw error;
};

/**
 * Requests a URL, returning a promise
 *
 * @param  {string} url       The URL we want to request
 * @param  {object} [options] The options we want to pass to "fetch"
 *
 * @return {object}           The response data
 */
export default (url, body, method = 'GET', type = 'json', { headers = {}, ...options } = {}) => {
  // Append backend url if url is relative
  const { REACT_APP_BACKEND_URL } = process.env;
  const apiURL = url.startsWith('/') ? `${REACT_APP_BACKEND_URL}/api${url}` : url;
  const formData = new FormData();
  const headerList = { ...headers };
  const opt = {
    headers: headerList,
    method,
    mode: 'cors',
    ...options,
  };
  if (body) {
    opt.body = type === 'form' ? formData : JSON.stringify(body);
  }
  return fetch(apiURL, opt).then(checkStatus).then(parseJSON);
};
