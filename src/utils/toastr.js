import toastr from 'toastr';

export const successAlert = (title = '', message = '', opts = {}) => {
  toastr.success(message, title, opts);
};
export const errorAlert = (title = '', message = '', opts = {}) => {
  toastr.error(message, title, opts);
};
