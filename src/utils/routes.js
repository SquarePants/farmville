/* eslint-disable */

import React, { Suspense } from 'react';
import { Switch } from 'react-router-dom';
import AuthRoute from '../components/authRoute';

const Product = React.lazy(() => import('../containers/Product'));
const Register = React.lazy(() => import('../containers/App/Register'));
const Login = React.lazy(() => import('../containers/App/Login'));

const Forest = React.lazy(() => import('../containers/Forest'));
const Farmers = React.lazy(() => import('../containers/Farmers'));
const Map = React.lazy(() => import('../containers/Map'));
const Routes = () => {
  return (
    <Suspense fallback={<div>Loading...</div>}>
      <Switch>
        <AuthRoute Component={Forest} path="/" exact role="Farmer" />
        <AuthRoute Component={Map} path="/map" exact />
        <AuthRoute Component={Login} path="/login" exact />
        <AuthRoute Component={Register} path="/register" exact />
        <AuthRoute Component={Product} path="/product" role="Farmer" />
        <AuthRoute Component={Forest} path="/forest" role="Farmer" />
        <AuthRoute Component={Farmers} path="/farmers" />
      </Switch>
    </Suspense>
  );
};
export default Routes;
