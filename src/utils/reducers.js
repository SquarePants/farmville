import { combineReducers } from 'redux';

import appReducer from '../containers/App/reducer';
import productReducer from '../containers/Product/reducer';
import forestReducer from '../containers/Forest/reducer';

export default combineReducers({
  appReducer,
  forestReducer,
  productReducer,
});
