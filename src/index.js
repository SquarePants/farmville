/* eslint-disable import/no-named-as-default */
/* eslint-disable import/no-named-as-default-member */
import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { BrowserRouter as Router } from 'react-router-dom';
import Routes from './utils/routes';
import configureStore from './utils/configureStore';
import history from './utils/history';
// styles + scripts
import './utils/styles';
import './utils/scripts';
import App from './containers/App';

const Index = () => {
  const store = configureStore({});

  return (
    <React.StrictMode>
      <Provider store={store}>
        <Router history={history}>
          <App>
            <Routes />
          </App>
        </Router>
      </Provider>
    </React.StrictMode>
  );
};
ReactDOM.render(<Index />, document.getElementById('root'));
