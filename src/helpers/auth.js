/* eslint-disable camelcase */
/* eslint-disable import/prefer-default-export */
import jwt_decode from 'jwt-decode';

export const decodeLocalUser = () => {
  const localToken = localStorage.getItem('token');
  if (!localToken) return null;
  return jwt_decode(localToken);
};
export const decodeConnectedUser = (token) => jwt_decode(token);
export const isConnected = () => {
  if (!localStorage.getItem('token')) return false;
  return true;
};
export const removeToken = () => {
  localStorage.removeItem('token');
};
