/* eslint-disable react/forbid-foreign-prop-types */
import checkPropTypes from 'check-prop-types';
import { applyMiddleware, createStore } from 'redux';
import ReduxThunk from 'redux-thunk';
import rootReducer from '../utils/reducers';

export const findByClassname = (component, attr) => {
  const wrapper = component.find(`.${attr}`);
  return wrapper;
};

export const checkProps = (component, expectedProps) => {
  const propsErr = checkPropTypes(component.propTypes, expectedProps, 'props', component.name);
  return propsErr;
};

export const testStore = (initialState) => createStore(rootReducer, initialState, applyMiddleware(ReduxThunk));
