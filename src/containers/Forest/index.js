import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import PropTypes from 'prop-types';
import { Container, Spinner, Row, Col } from 'reactstrap';
import { getForest } from './actions';
import Tree from './Tree';
import Wood from './Wood';
import { decodeLocalUser } from '../../helpers/auth';
import Cycle from './Cycle';
import House from './House';
import Map from './Map';
import Pieces from './Pieces';

function Forest({ farmer = decodeLocalUser().id }) {
  const dispatch = useDispatch();
  const { isFetching } = useSelector((state) => state.forestReducer);
  useEffect(() => {
    dispatch(getForest(farmer));
  }, [dispatch, farmer]);
  if (isFetching) {
    return <Spinner color="success" />;
  }
  return (
    <Container>
      <br />
      <br />
      <Cycle farmerId={farmer} />
      <hr />
      <br />
      <br />
      <Row>
        <Col>
          <Tree />
        </Col>
        <Col>
          <Wood />
        </Col>
        <Col>
          <House />
        </Col>
        <Col>D</Col>
      </Row>
      <hr />
      <br />
      <br />
      <Map />
      <hr />
      <Row>
        <Pieces />
      </Row>
    </Container>
  );
}

Forest.propTypes = {
  farmer: PropTypes.string,
};
Forest.defaultProps = {
  farmer: decodeLocalUser().id,
};
export default Forest;
