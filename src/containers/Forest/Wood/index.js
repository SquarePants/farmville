import React from 'react';
import { Badge, Button } from 'reactstrap';
import { useSelector, useDispatch } from 'react-redux';
import { collectWood } from '../actions';

function Wood() {
  const dispatch = useDispatch();
  const { error, isFetching, localWoodCount, localTreeCount, forest } = useSelector((state) => state.forestReducer);
  const collectShit = () => {
    if (localTreeCount > 0) {
      dispatch(collectWood(forest._id));
    }
  };
  return (
    <div>
      {error && <p>{error}</p>}
      <h1>
        <Badge color="secondary">{localWoodCount}</Badge>
      </h1>
      <Button
        color="success"
        onClick={(e) => {
          e.preventDefault();
          collectShit();
        }}
        disabled={isFetching || localTreeCount === 0}
      >
        Collect wood
      </Button>
    </div>
  );
}

export default Wood;
