/* eslint-disable react/jsx-one-expression-per-line */
import React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { Button, Badge } from 'reactstrap';
import { plantTree, plantLocally } from '../actions';

function Tree() {
  const dispatch = useDispatch();
  const { error, isFetching, localTreeCount, forest } = useSelector((state) => state.forestReducer);
  const plantShit = () => {
    dispatch(plantLocally());

    dispatch(plantTree(forest._id));
  };
  return (
    <div>
      {error && <p>{error}</p>}
      <h1>
        <Badge color="secondary">{localTreeCount}</Badge>{' '}
      </h1>
      <Button
        color="success"
        onClick={(e) => {
          e.preventDefault();
          plantShit();
        }}
        disabled={isFetching}
      >
        Plant a tree
      </Button>
    </div>
  );
}

export default Tree;
