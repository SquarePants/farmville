import produce from 'immer';
import * as types from './constants';

/* eslint-disable default-case, no-param-reassign, consistent-return */
export default function forestReducer(
  state = {
    error: null,
    isFetching: false,
    forest: {},
    localTreeCount: 0,
    localWoodCount: 0,
    localHouseCount: 0,
  },
  action
) {
  return produce(state, (draft) => {
    switch (action.type) {
      case types.FETCHING:
        draft.isFetching = true;
        draft.error = null;
        break;
      case types.GET_FOREST_SUCCESS:
        draft.forest = action.forest;
        draft.localTreeCount = action.forest.treeCount;
        draft.localHouseCount = action.forest.houseCount;
        draft.localWoodCount = action.forest.woodCount;
        draft.isFetching = false;
        break;
      case types.PLANT_TREE_SUCCESS:
        draft.forest.treeCount += 1;
        draft.isFetching = false;
        break;
      case types.PLANT_TREE_FAILURE:
        draft.error = action.error;
        draft.isFetching = false;
        draft.localTreeCount += 1;
        break;
      case types.COLLECT_WOOD_SUCCESS:
        draft.forest.woodCount += 5;
        draft.forest.treeCount -= 1;
        draft.localWoodCount += 5;
        draft.localTreeCount -= 1;
        draft.isFetching = false;
        break;
      case types.COLLECT_WOOD_FAILURE:
        draft.error = action.error;
        draft.isFetching = false;
        draft.localWoodCount += 5;
        draft.localTreeCount -= 1;
        break;
      case types.GET_FOREST_FAILURE:
        draft.isFetching = false;
        draft.error = action.error;
        break;
      case types.BUILD_HOUSE_SUCCESS:
        draft.forest.houseCount += 1;
        draft.localHouseCount += 1;
        draft.localWoodCount -= 1000;
        draft.forest.woodCount -= 1000;
        draft.isFetching = false;
        break;
      case types.BUILD_HOUSE_FAILURE:
        draft.error = action.error;
        draft.isFetching = false;
        draft.localWoodCount -= 1000;
        draft.localHouseCount += 1;
        break;
      case types.PLANT_TREE_LOCALLY:
        draft.localTreeCount += 1;
        break;

      default:
        return state;
    }
  });
}
