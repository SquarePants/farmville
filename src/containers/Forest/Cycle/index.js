import React, { useEffect, useState } from 'react';
import PropTypes from 'prop-types';
import { Spinner } from 'reactstrap';
import socketManager from '../../../utils/socket';
import ProgressBar from '../../../components/progressbar';
import Timer from '../../../components/timer';

function Cycle({ farmerId }) {
  const [socket, setSocket] = useState(null);
  const [connected, setConnected] = useState(false);
  const [error, setError] = useState(null);
  const [value, setValue] = useState(null);
  useEffect(() => {
    if (socket === null) {
      setSocket(socketManager.getInstance());
    }
    return () => {
      if (socket !== null) {
        socket.disconnect();
        setSocket(null);
      }
    };
  }, []);
  useEffect(() => {
    if (socket !== null) {
      socket.emit('auth::request', { farmerId });
      socket.on('auth::ok', () => {
        setConnected(true);
      });
      socket.on('auth::nope', ({ message }) => {
        setError(message);
      });
      socket.on('progress::update', ({ timeleft }) => {
        console.log(timeleft);
        setValue(timeleft);
      });
    }
  }, [farmerId, socket]);
  useEffect(() => {
    if (value < 1 && socket) {
      socket.emit('progress::renew');
    }
  });
  if (!connected) {
    return <Spinner color="success" />;
  }
  if (error) {
    return <p color="red">{error}</p>;
  }
  if (value) {
    return (
      <div>
        <div className="text-center">
          next cycle in
          <b>
            <Timer countdown={value} setValue={setValue} />
          </b>
        </div>
        <ProgressBar value={60 - value} max={60} min={0} animated />
      </div>
    );
  }
  return <Spinner />;
}
Cycle.propTypes = {
  farmerId: PropTypes.string.isRequired,
};

export default Cycle;
