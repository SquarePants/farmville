import { call, put, all, takeLatest } from 'redux-saga/effects';
import axios from 'axios';
import * as actions from './actions';
import * as types from './constants';
import request from '../../utils/request';

const get = (url) => axios.get(`${process.env.REACT_APP_BACKEND_URL}/api${url}`, { headers: { 'Content-Type': 'application/json' } });

function* fetchForest({ farmer }) {
  yield put(actions.fetching());
  try {
    const response = yield call(get, `/forest/${farmer}`);
    yield put(actions.getForestSuccess(response.data.forest));
    return response;
  } catch (error) {
    yield put(actions.getForestError('Coudnt find forest'));
    return false;
  }
}
function* plantTree({ forest }) {
  try {
    const response = yield call(request, `/forest/plant/${forest}`, null, 'GET');
    yield put(actions.plantTreeSuccess());
    return response;
  } catch (error) {
    yield put(actions.plantTreeFailure('nope'));
    return false;
  }
}
function* collectWood({ forest }) {
  try {
    const response = yield call(request, `/forest/collect/${forest}`, null, 'GET');
    yield put(actions.collectWoodSuccess());
    return response;
  } catch (error) {
    yield put(actions.collectWoodFailure('nope'));
    return false;
  }
}
function* buildHouse({ forest }) {
  try {
    const response = yield call(request, `/forest/build/${forest}`, null, 'GET');
    yield put(actions.buildHouseSuccess());
    return response;
  } catch (error) {
    yield put(actions.buildHouseFailure('nope'));
    return false;
  }
}
export default function* rootSaga() {
  yield all([
    takeLatest(types.GET_FOREST, fetchForest),
    takeLatest(types.PLANT_TREE, plantTree),
    takeLatest(types.COLLECT_WOOD, collectWood),
    takeLatest(types.BUILD_HOUSE, buildHouse),
  ]);
}
