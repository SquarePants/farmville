/* eslint-disable import/prefer-default-export */

import * as types from './constants';

export const getForestSuccess = (forest) => ({
  type: types.GET_FOREST_SUCCESS,
  forest,
});
export const getForestError = (error) => ({
  type: types.GET_FOREST_FAILURE,
  error,
});
export const plantTreeSuccess = () => ({
  type: types.PLANT_TREE_SUCCESS,
});
export const plantTreeFailure = (error) => ({
  type: types.PLANT_TREE_FAILURE,
  error,
});
export const getForest = (farmer) => ({
  type: types.GET_FOREST,
  farmer,
});
export const plantTree = (forest) => ({
  type: types.PLANT_TREE,
  forest,
});
export const plantLocally = () => ({
  type: types.PLANT_TREE_LOCALLY,
});
export const fetching = () => ({
  type: types.FETCHING,
});
export const collectWood = (forest) => ({
  type: types.COLLECT_WOOD,
  forest,
});
export const collectWoodSuccess = () => ({
  type: types.COLLECT_WOOD_SUCCESS,
});
export const collectWoodFailure = (error) => ({
  type: types.COLLECT_WOOD_FAILURE,
  error,
});
export const buildHouse = (forest) => ({
  type: types.BUILD_HOUSE,
  forest,
});
export const buildHouseSuccess = () => ({
  type: types.BUILD_HOUSE_SUCCESS,
});
export const buildHouseFailure = (error) => ({
  type: types.BUILD_HOUSE_FAILURE,
  error,
});
