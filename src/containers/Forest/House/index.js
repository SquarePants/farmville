import React from 'react';
import { Badge, Button } from 'reactstrap';
import { useSelector, useDispatch } from 'react-redux';
import { buildHouse } from '../actions';

function House() {
  const dispatch = useDispatch();
  const { error, isFetching, localWoodCount, localHouseCount, forest } = useSelector((state) => state.forestReducer);
  const buildShit = () => {
    if (localWoodCount > 999) {
      dispatch(buildHouse(forest._id));
    }
  };
  return (
    <div>
      {error && <p>{error}</p>}
      <h1>
        <Badge color="secondary">{localHouseCount}</Badge>
      </h1>
      <Button
        color="success"
        onClick={(e) => {
          e.preventDefault();
          buildShit();
        }}
        disabled={isFetching || localWoodCount < 1000}
      >
        Build House
      </Button>
    </div>
  );
}

export default House;
