import React, { useState, useEffect } from 'react';
import Square from './Map/square';
import Knight from './Map/knight';
import Board from './Map/board';

function Farmers() {
  const [knightPosition, setKnightPosition] = useState([0, 0]);

  let observer = null;

  function emitChange() {
    observer(knightPosition);
  }

  function observe(o) {
    if (observer) {
      throw new Error('Multiple observers not implemented.');
    }

    observer = o;
    emitChange();
  }

  function moveKnight(toX, toY) {
    setKnightPosition([toX, toY]);
    emitChange();
  }
  useEffect(() => {
    observe((kp) => setKnightPosition(kp));
  });
  return <Board knightPosition={knightPosition} />;
}

export default Farmers;
