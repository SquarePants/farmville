import React from 'react';

export default function Square({ gray, children }) {
  const fill = gray ? 'gray' : 'white';
  const stroke = gray ? 'white' : 'gray';

  return (
    <div
      style={{
        backgroundColor: fill,
        color: stroke,
        width: '100%',
        height: '100%',
      }}
    >
      {children}
    </div>
  );
}
