import React from 'react';
import FarmerIcon from '../../assets/icons/farmers/farmer.svg';

export default function Farmer() {
  return <img alt="Some farmer " src={FarmerIcon} style={{ width: '50px', height: '50px' }} />;
}
