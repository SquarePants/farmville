import React from 'react';
import Farmer from './Farmer';
import Square from './Square';

const Map = () => (
  <Square gray>
    <Farmer />
  </Square>
);

export default Map;
