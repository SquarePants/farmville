import { createSelector } from 'reselect';
import { decodeLocalUser, isConnected, decodeConnectedUser } from '../../helpers/auth';

const selectApp = (state) => state.appReducer;

export const selectAppState = createSelector(selectApp, (appState) => appState);
export const selectFetching = createSelector(selectApp, ({ isFetching }) => isFetching);
export const selectError = createSelector(selectApp, ({ error }) => error);
export const selectUser = createSelector(selectApp, ({ token }) => {
  if (isConnected()) {
    return decodeLocalUser();
  }
  return decodeConnectedUser(token) || null;
});
