import * as types from './constants';

export const login = (data, history) => ({
  type: types.LOGIN,
  data,
  history,
});
export const loginSuccess = (token) => ({
  type: types.LOGIN_SUCCESS,
  token,
});
export const error = (err) => ({
  type: types.LOGIN_ERROR,
  error: err,
});

export const register = (data, history) => ({
  type: types.REGISTER,
  data,
  history,
});
export const registerSuccess = () => ({
  type: types.REGISTER_SUCCESS,
});
