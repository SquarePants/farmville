import { call, put, all, takeLatest } from 'redux-saga/effects';
import axios from 'axios';
import * as actions from './actions';
import * as types from './constants';
import * as alerts from '../../utils/toastr';
import { successAlert, errorAlert } from '../../utils/toastr';

const post = (url, body) => axios.post(`${process.env.REACT_APP_BACKEND_URL}/api${url}`, body, { headers: { 'Content-Type': 'application/json' } });

function* loginUser({ data, history }) {
  try {
    const response = yield call(post, '/user/login', data);
    yield put(actions.loginSuccess(response));
    if (response.data.success === true) {
      yield call(alerts.successAlert, 'Login', 'Welcome back');
      localStorage.setItem('token', response.data.token);
      history.push('/');
    } else {
      yield call(alerts.errorAlert, 'Login Error', 'Invalid credentials');
    }
    return response;
  } catch (error) {
    yield put(actions.error('error'));
    return false;
  }
}
function* registerUser({ data, history }) {
  try {
    const response = yield call(post, '/user/register', data);
    if (response.data.success === true) {
      yield put(actions.registerSuccess());
      yield call(successAlert, 'Your account has been successfully created', 'Registration Complete');
      history.push('/login');
    } else {
      yield put(actions.error(response.data.error));
      yield call(errorAlert, 'Error during registration. Please try again', 'Registration Failed');
    }
    return response;
  } catch (error) {
    yield put(actions.error('error'));
    return false;
  }
}

export default function* rootSaga() {
  yield all([takeLatest(types.LOGIN, loginUser), takeLatest(types.REGISTER, registerUser)]);
}
