import React from 'react';
import { Col, Button, Form, FormGroup, Label, Input, FormText } from 'reactstrap';
import { useDispatch, useSelector } from 'react-redux';
import { useHistory } from 'react-router-dom';
import { useFormik } from 'formik';
import { register } from '../actions';
import { selectAppState } from '../selectors';
import { registerSchema } from '../components/validators';

function Register() {
  const dispatch = useDispatch();
  const history = useHistory();
  const { isFetching, error } = useSelector(selectAppState);
  const { handleSubmit, handleChange, errors } = useFormik({
    initialValues: {
      name: '',
      email: '',
      password: '',
    },
    validationSchema: registerSchema,
    onSubmit({ name, email, password }) {
      dispatch(register({ name, email, password }, history));
    },
  });

  return (
    <Col sm="6">
      <Form onSubmit={handleSubmit}>
        <FormGroup>
          <Label for="name">Name</Label>
          <Input type="text" name="name" placeholder="My Name" onChange={handleChange} />
          {errors.name && <FormText color="danger">{errors.name}</FormText>}
        </FormGroup>
        <FormGroup>
          <Label for="variety">Email</Label>
          <Input type="text" name="email" placeholder="example@example.io" onChange={handleChange} />
          {errors.email && <FormText color="danger">{errors.email}</FormText>}
        </FormGroup>
        <FormGroup>
          <Label for="image">Password</Label>
          <Input type="password" name="password" onChange={handleChange} />
          {errors.password && <FormText color="danger">{errors.password}</FormText>}
        </FormGroup>
        {error && <FormText color="danger">{error}</FormText>}
        <Button data-testid="input-submit" type="submit" disabled={isFetching}>
          Submit
        </Button>
      </Form>
    </Col>
  );
}

export default Register;
