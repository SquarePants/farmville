import produce from 'immer';
import * as types from './constants';

/* eslint-disable default-case, no-param-reassign, consistent-return */
export default function registerReducer(
  state = {
    error: null,
    isFetching: false,
    token: {},
  },
  action
) {
  return produce(state, (draft) => {
    switch (action.type) {
      case types.LOGIN:
        draft.isFetching = true;
        break;
      case types.LOGIN_ERROR:
        draft.error = action.error;
        draft.isFetching = false;
        break;
      case types.LOGIN_SUCCESS:
        draft.token = action.payload;
        draft.isFetching = false;
        break;
      case types.REGISTER:
        draft.isFetching = true;
        break;
      case types.REGISTER_ERROR:
        draft.error = action.error;
        draft.isFetching = false;
        break;
      case types.REGISTER_SUCCESS:
        draft.token = action.payload;
        draft.isFetching = false;
        break;
      default:
        return state;
    }
  });
}
