import React from 'react';
import { Col, Button, Form, FormGroup, Label, Input, FormText } from 'reactstrap';
import { useDispatch, useSelector } from 'react-redux';
import { useHistory } from 'react-router-dom';
import { useFormik } from 'formik';
import { login } from '../actions';
import { loginSchema } from '../components/validators';
import { selectAppState } from '../selectors';

function Login() {
  const dispatch = useDispatch();
  const history = useHistory();
  const { isFetching, error } = useSelector(selectAppState);
  const { handleSubmit, handleChange, errors } = useFormik({
    initialValues: {
      email: '',
      password: '',
    },
    validationSchema: loginSchema,
    onSubmit({ email, password }) {
      dispatch(login({ email, password }, history));
    },
  });
  return (
    <Col sm="6">
      <Form onSubmit={handleSubmit}>
        <FormGroup>
          <Label for="variety">Email</Label>
          <Input type="text" name="email" id="email" placeholder="example@example.io" onChange={handleChange} />
          {errors.email && <FormText color="danger">{errors.email}</FormText>}
        </FormGroup>
        <FormGroup>
          <Label for="image">Password</Label>
          <Input type="password" name="password" id="password" onChange={handleChange} />
          {errors.password && <FormText color="danger">{errors.password}</FormText>}
        </FormGroup>
        {error && <FormText color="danger">{error}</FormText>}
        <Button type="submit" data-testid="input-submit" disabled={isFetching}>
          Submit
        </Button>
      </Form>
    </Col>
  );
}

export default Login;
