import React from 'react';
import PropTypes from 'prop-types';
import { Container } from 'reactstrap';
import Navbar from './navbar';
import Footer from './footer';
/**
 *
 *  Handles navbar / alerts / toastr / user state
 *
 */
const App = ({ children }) => (
  <Container className="App">
    <Navbar />
    {children}
    <Footer />
  </Container>
);
App.propTypes = {
  children: PropTypes.shape({}).isRequired,
};
export default App;
