import React, { useState } from 'react';
import { Collapse, Navbar, NavbarToggler, NavbarBrand, Nav, NavItem, NavLink } from 'reactstrap';
import { useHistory } from 'react-router-dom';
import { isConnected, removeToken } from '../../helpers/auth';

const NavBar = () => {
  const [isOpen, setIsOpen] = useState(false);
  const history = useHistory();
  const toggle = () => setIsOpen(!isOpen);
  const disconnect = () => {
    removeToken();
    history.push('/');
  };
  return (
    <header className="App-header">
      <Navbar color="light" light expand="md">
        <NavbarBrand href="/">Farmville</NavbarBrand>
        <NavbarToggler onClick={toggle} />
        <Collapse isOpen={isOpen} navbar>
          <Nav className="mr-auto" navbar>
            <NavItem>
              <NavLink href="/forest">Forest</NavLink>
            </NavItem>
            <NavItem>
              <NavLink href="/farmers">Farmers</NavLink>
            </NavItem>
          </Nav>
          <Nav>
            {isConnected() ? (
              <>
                <NavItem>
                  <NavLink
                    onClick={() => {
                      disconnect();
                    }}
                    href="/disconnect"
                  >
                    Disconnect
                  </NavLink>
                </NavItem>
              </>
            ) : (
              <>
                <NavItem>
                  <NavLink href="/login">Login</NavLink>
                </NavItem>
                <NavItem>
                  <NavLink href="/register">Register</NavLink>
                </NavItem>
              </>
            )}
          </Nav>
        </Collapse>
      </Navbar>
    </header>
  );
};

export default NavBar;
