import * as Yup from 'yup';

export const loginSchema = Yup.object().shape({
  email: Yup.string().required('Email is required').email('Invalid Email'),
  password: Yup.string().min(5, 'Password must be at least 5 characters long'),
});
export const registerSchema = Yup.object().shape({
  name: Yup.string().required('Doesnt have to be your real name'),
  email: Yup.string().required('Any mail will do').email('Invalid Email Format'),
  password: Yup.string().required('a password to log into your account').min(5, 'More letters pls'),
});
