import React, { useState } from 'react';
import { Col, Button, Form, FormGroup, Label, Input, FormText } from 'reactstrap';
import { useDispatch } from 'react-redux';
import { addProduct } from '../actions';

function AddProduct() {
  const dispatch = useDispatch();
  const [name, setName] = useState('');
  const [variety, setVariety] = useState('');
  const [images, setImages] = useState('');

  const submitForm = () => {
    const formData = new FormData();
    formData.append('name', name);
    formData.append('variety', variety);
    formData.append('images', images);
    formData.append('type', 'FRUIT');
    dispatch(addProduct(formData));
  };
  return (
    <Col sm="6">
      <Form>
        <FormGroup>
          <Label for="name">Name</Label>
          <Input
            data-testid="input-name"
            type="text"
            name="name"
            id="name"
            placeholder="Product Name"
            onChange={(e) => {
              setName(e.target.value);
            }}
          />
        </FormGroup>
        <FormGroup>
          <Label for="variety">Variety</Label>
          <Input
            data-testid="input-variety"
            type="text"
            name="variety"
            id="variety"
            placeholder="Product Variety"
            onChange={(e) => {
              setVariety(e.target.value);
            }}
          />
        </FormGroup>
        <FormGroup>
          <Label for="image">Image</Label>
          <Input
            data-testid="input-image"
            type="file"
            name="image"
            id="image"
            onChange={(e) => {
              setImages(e.target.files);
            }}
            multiple
          />
          <FormText color="muted">Only png, jpeg and jpg are allowed. Max size: 2mb</FormText>
        </FormGroup>
        <Button
          data-testid="input-submit"
          onClick={(e) => {
            e.preventDefault();
            submitForm();
          }}
        >
          Submit
        </Button>
        <Button
          onClick={(e) => {
            e.preventDefault();
            dispatch(addProduct(new FormData()));
          }}
        >
          TEST
        </Button>
      </Form>
    </Col>
  );
}

export default AddProduct;
