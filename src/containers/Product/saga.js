import { call, put, all, takeLatest } from 'redux-saga/effects';
import axios from 'axios';
import * as actions from './actions';
import * as types from './constants';
import request from '../../utils/request';

const post = (url, body) => axios.post(`${process.env.REACT_APP_BACKEND_URL}/api${url}`, body, { headers: { 'Content-Type': 'application/json' } });
function* fetchProductList() {
  yield put(actions.fetching());
  try {
    const response = yield call(request, '/product', null, 'GET');
    yield put(actions.fetchProductListSuccess(response));
    return response;
  } catch (error) {
    yield put(actions.requestError('error'));
    return false;
  }
}
function* addProduct({ data }) {
  try {
    const response = yield post('/product', data);
    return response;
  } catch (error) {
    yield put(actions.requestError('error'));
    return false;
  }
}
export default function* rootSaga() {
  yield all([takeLatest(types.FETCH_PRODUCTLIST, fetchProductList), takeLatest(types.ADD_PRODUCT, addProduct)]);
}
