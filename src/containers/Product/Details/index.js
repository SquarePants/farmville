import React from 'react';
import PropTypes from 'prop-types';
import { Card, CardBody, Button, CardTitle, CardText, Col } from 'reactstrap';
import ImageThumbnail from '../../../components/imagethumbnail';

function Details({ product }) {
  return (
    <Col sm="6">
      <Card>
        <CardBody>
          <CardTitle className="product-name">{product.name}</CardTitle>
        </CardBody>
        {product.images.map((image) => (
          <ImageThumbnail
            key={image._id}
            classname="product-image"
            imageId={image._id}
            alt={image.name}
            customStyle={{ width: '100px', height: '100px', align: 'center' }}
          />
        ))}

        <CardBody>
          <CardText className="product-variety">{product.variety}</CardText>
          <Button>Go somewhere</Button>
        </CardBody>
      </Card>
    </Col>
  );
}

export default Details;
Details.propTypes = {
  product: PropTypes.shape({
    name: PropTypes.string,
    variety: PropTypes.string,
    images: PropTypes.arrayOf(
      PropTypes.shape({
        _id: PropTypes.string,
      })
    ),
  }).isRequired,
};
