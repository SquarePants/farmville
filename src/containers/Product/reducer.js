import produce from 'immer';
import { ERROR, FETCHING, FETCH_PRODUCTLIST_SUCCESS } from './constants';

/* eslint-disable default-case, no-param-reassign, consistent-return */
export default function productReducer(
  state = {
    error: null,
    isFetching: false,
    productList: [],
  },
  action
) {
  return produce(state, (draft) => {
    switch (action.type) {
      case ERROR:
        draft.error = action.error;
        break;
      case FETCHING:
        draft.isFetching = true;
        break;
      case FETCH_PRODUCTLIST_SUCCESS:
        draft.productList = action.productList;
        draft.isFetching = false;
        break;
      default:
        return state;
    }
  });
}
