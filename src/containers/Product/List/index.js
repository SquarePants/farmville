import React from 'react';
import PropTypes from 'prop-types';
import { Row } from 'reactstrap';
import ProductDetails from '../Details';

function List({ productList }) {
  return (
    <div>
      {productList.map((product) => (
        <Row key={product._id} data-testid="product-list">
          <ProductDetails product={product} />
        </Row>
      ))}
    </div>
  );
}
List.propTypes = {
  productList: PropTypes.arrayOf(PropTypes.shape({})).isRequired,
};
export default List;
