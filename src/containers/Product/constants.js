export const ERROR = 'ERROR';
export const FETCHING = 'FETCHING';
export const FETCH_PRODUCTLIST = 'FETCH_PRODUCTLIST';
export const FETCH_PRODUCTLIST_SUCCESS = 'FETCH_PRODUCTLIST_SUCCESS';
export const ADD_PRODUCT = 'ADD_PRODUCT';
