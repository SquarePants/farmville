/* eslint-disable import/prefer-default-export */

import * as types from './constants';

export const fetchProductListSuccess = (productList) => ({
  type: types.FETCH_PRODUCTLIST_SUCCESS,
  productList,
});
export const fetchProductList = () => ({
  type: types.FETCH_PRODUCTLIST,
});
export const requestError = (error) => ({
  type: types.ERROR,
  error,
});
export const fetching = () => ({
  type: types.FETCHING,
});
export const addProduct = (formdata) => ({
  type: types.ADD_PRODUCT,
  data: formdata,
});
