import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { Spinner } from 'reactstrap';
import ProductList from './List';
import NewProduct from './Add';
import { fetchProductList } from './actions';

function Product() {
  const dispatch = useDispatch();
  const { isFetching, error, productList } = useSelector((state) => state.productReducer);
  useEffect(() => {
    dispatch(fetchProductList());
  }, [dispatch]);
  if (isFetching) {
    return (
      <div className="product" data-testid="product-component">
        <Spinner />
      </div>
    );
  }
  return (
    <div className="product" data-testid="product-component">
      <NewProduct />
      {error ? <div>{error}</div> : <ProductList productList={productList} />}
    </div>
  );
}

export default Product;
